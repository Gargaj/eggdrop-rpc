<?php
ini_set("display_errors",1);
include_once("rpc.inc.php");

header("Content-type: text/plain");

$rpc = new EggdropRPC();

echo "\n\nConnecting...\n";
$response = $rpc->connect("localhost",60607);
print_r($response);

if ($response[2] == "plaintext-password")
{
  echo "\n\nAuthenticating via plaintext...\n";
  $response = $rpc->auth("Serious business consulting Ltd");
  var_dump($response);
}

$text = sha1(time());
echo "\n\nEcho '".$text."' ...\n";
$response = $rpc->echoText($text);
var_dump($response);

echo "\n\nList methods ...\n";
$response = $rpc->methods();
print_r($response);

echo "\n\nCheck password (some:pass - incorrect)...\n";
$response = $rpc->checkpass("some","pass");
var_dump($response);

echo "\n\nCheck password (ente`:thechosenone - correct)...\n";
$response = $rpc->checkpass("ente`","thechosenone");
var_dump($response);

echo "\n\nGet user data (ente`, LASTON)...\n";
$response = $rpc->getuser("ente`","LASTON");
print_r($response);

echo "\n\nGet user data (ente`, HOSTS)...\n";
$response = $rpc->getuser("ente`","HOSTS");
print_r($response);

?>