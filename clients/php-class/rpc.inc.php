<?php
class EggdropRPC
{
  private $socket;
  function connect( $host, $port = 1337 )
  {
    $this->socket = fsockopen($host,$port);
    if ($this->socket === false)
      return false;
    
    return $this->readResponse();
  }
  function disconnect()
  {
    fclose($this->socket);
  }
  function readResponse()
  {
    $response = "";
    while (!feof($this->socket) && strstr($response,"\0\0\0\0") === false) 
      $response .= fread($this->socket, 128);
    
    return explode("\0",trim($response,"\0"));
  }
  function sendCommand( $cmdArray )
  {
    fwrite( $this->socket, implode("\0",$cmdArray) . "\0\0\0\0" );
  }

  ////////////////////////////////////////
  // methods
  function auth( $secret )
  {
    $this->sendCommand( array("authenticate",$secret) );
    $response = $this->readResponse();
    return $response[0] == "OK";
  }
  function echoText( $text )
  {
    $this->sendCommand( array("echo",$text) );
    $response = $this->readResponse();
    return $response[1];
  }
  function methods()
  {
    $this->sendCommand( array("methods") );
    $response = $this->readResponse();
    return array_slice($response,1);
  }
  function checkpass( $user, $pass )
  {
    $this->sendCommand( array("checkpass",$user,$pass) );
    $response = $this->readResponse();
    return $response[1] != 0;
  }  
  function changepass( $user, $pass )
  {
    $this->sendCommand( array("changepass",$user,$pass) );
    $response = $this->readResponse();
    return $response[1] != 0;
  }  
  function getuser( $user, $entry )
  {
    $this->sendCommand( array("getuser",$user,$entry) );
    $response = $this->readResponse();
    return array_slice($response,1);
  }  
}
?>