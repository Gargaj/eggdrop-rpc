#include <stdlib.h>
#include <stdio.h>
#include "rpc.h"

void printrpc(char const **rpc, size_t len)
{
	size_t i;

	printf("%zd members:\n", len);
	for (i = 0; rpc[i]; i++)
		printf("%zd: %s\n", i, rpc[i]);
}

int main()
{
	char const mesg[] = "echo\0hello\0\0world\0\0\0\0\0";
	char const mesganother[] = "\0\0\0\0";

	char const **rpc, **rpc2;
	char *mesg2;
	size_t len, len2;
	
	rpc = rpc_split(mesg, sizeof(mesg), &len2);
	if (!rpc) {
		printf("error parsing message, this shouldn't happen\n");
		return 1;
	}

	printrpc(rpc, len2);

	mesg2 = rpc_format(rpc, &len);     /* now serialize the message again */

	printf("rpc length %zd\n", len);/* let's just print the length of that*/

	rpc2 = rpc_split(mesg2, len, &len2);              /* split it back up */

	printrpc(rpc2, len2);

	mesg2[len-1] = 'a';             /* turn it into an incomplete message */

	if (rpc_split(mesg2, len, NULL) != NULL) {
		printf("error: incomplete message was split regardless...\n");
	}

	free(rpc);                          /* assuming we're done processing */
	free(mesg2);
	free(rpc2);

	return 0;
}
