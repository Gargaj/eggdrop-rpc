#!/usr/bin/tclsh
#
# Connects to the server, authenticates, reads Tcl formatted lists on stdin and
# sends them to the server in RPC format, sits there waiting for a response.
#
# run as
# 	./testclient -port 4711 -server example.com -tls yes -hex yes
#
# See array below for options

array set cfg {
	server		localhost
	port		1337
	password 	"Satan Calculus is coming to town"
	tls		no
	color		yes
	hex		yes
}

lappend auto_path [file dirname [info script]]

package require Tcl 8.5
package require EggdropRPC 0.1
package require SASL
package require base64
package require term::ansi::send 

proc nop args {}

proc b64dec {arg} {
        # wrapper around base64::decode that verifies that anything can be
        # base64 at all.
        set r {^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$}
        if {![regexp $r $arg] || [catch {base64::decode $arg} b64]} {
                error "invalid base64 data"
        }
        return $b64
}

proc b64enc {arg} {
        base64::encode -maxlen 0 $arg
}


proc hexdump {arg} {
	set hd [open "|od -A x -t x1z -v" w]
	fconfigure $hd -encoding binary -translation binary
	puts -nonewline $hd $arg
	close $hd
}

proc in/out {data {inout "incoming"}} {
	if {$inout != "outgoing"} sda_fgred else sda_fgblue
	sda_bold
	puts "[clock format [clock seconds] -format %H:%M:%S] $inout"
	sda_nobold
	if {$::cfg(hex)} {
		hexdump $data
	}
	sda_reset
}

proc saslauth_callback {ctx command} {
	switch -exact -- $command {
		login    { return "" }
		username { return "" }
		password { return $::cfg(password) }
		realm    { return "" }
		hostname { return [info host] }
		default  { return -code error unexpected }
	}
}

proc authenticate {data} {
	lassign $data a0 a1 a2 a3

	if {$a0 eq "ERROR"} {
		puts "!!! error trying to authenticate"
		exit 1
	} elseif {$a0 eq "OK"} {
		set ::authenticated 1
		return
	} elseif {$a0 ne "AUTHENTICATION"} {
		puts "!!! protocol error, authentication expected."
		exit 1
	}

	if {$a2 eq "plaintext-password"} {
		if {$a1 ne "BEGIN"} {
			puts "!!! plaintext-auth only works with BEGIN"
			exit 1
		}
		myput [list authenticate $::cfg(password)]
		return
	} elseif {$a1 eq "BEGIN"} {
		set ::sasl [SASL::new \
			-service "eggdrop-rpc"                         \
			-mechanism $a2                                 \
			-callback saslauth_callback]
	}
	if {$::sasl != "" && $a1 in {"BEGIN" "CONTINUE"}} {
		SASL::step $::sasl [b64dec $a3]
		set par [join [b64enc [SASL::response $::sasl]]]
		myput [list authenticate $par]
		return
	}
	puts "!!! something is fishy with SASL"
	exit 1
}

proc mysplit {buf data} {
	in/out $data incoming
	rpc::split $buf $data
}

proc myput {data} {
	in/out [rpc::put $::s $data] outgoing
	puts "> $data"
}

proc read_socket {args} {
	global s authenticated

	if {[catch {read $s} data] || [eof $s]} {
		exit
	}
	
	set data [mysplit ::inbuf $data]
	if {$data eq ""} {
		return
	}
	puts "< $data"
	if {!$authenticated} {
		authenticate $data
	}
}

proc read_stdin {} {
	gets stdin msg
	if {[eof stdin]} { exit }
	if {[catch {myput $msg} err]} {
		puts $err
	}
}

foreach {k v} $argv {
	if {[string match {-*} $k]} {
		set cfg([string range $k 1 end]) $v
	} else {
		puts "syntax error: $k"
		exit 1
	}
}

# global variables
set inbuf {}
set authenticated 0
set dirty 0 ;# did the server respond to our previous query so we can send right away?
set sasl {}

if {$cfg(color)} {
	namespace import ::term::ansi::send::sda_*
} else {
	foreach c [info commands term::ansi::send::sda_*] {
		regexp {.*::(sda_.*)} $c -> basename
		interp alias {} $basename {} nop
	}
}

if {$cfg(tls)} {
	package require tls
	set s [tls::socket -ssl2 0 -ssl3 0 -tls1 1 $cfg(server) $cfg(port)]
	myput nop
} else {
	set s [socket $cfg(server) $cfg(port)]
}


fconfigure $s -blocking 0 -encoding binary -translation binary

fileevent $s    readable read_socket
fileevent stdin readable read_stdin

vwait forever
