#!/usr/bin/env perl

package Text::EggdropRPC;

use strict;
use warnings;
use vars qw($VERSION @ISA @EXPORT);

require Exporter;
$VERSION = 0.001;
@ISA = qw(Exporter);
@EXPORT = qw(rpc_split rpc_format rpc_complete);

# returns 1 if the RPC data is complete, 0 otherwise.
sub rpc_complete {
	my $msg = shift;
	my $trailer = substr $msg, -4;
	return $trailer eq "\0\0\0\0";
}

# returns 0 on incomplete data, data as a list on complete data
# this could happen if the write syscall to the socket was interrupted, hence
# the trailer needs to be checked so that all RPC calls are always valid
sub rpc_split {
	my $msg = shift;
	return 0 unless rpc_complete $msg;
	$msg = substr $msg, 0, -4;
	return split /\0/, $msg;
}

# Format a list in such a way that it can be sent to the RPC server
# None of the list items may contain nul bytes.
sub rpc_format {
	return join("\0", @_) . "\0\0\0\0";
}

1
